﻿using System;

namespace Day26
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] actual = Console.ReadLine().Split(' ');
            string[] expected = Console.ReadLine().Split(' ');

            int aDay = int.Parse(actual[0]);
            int eDay = int.Parse(expected[0]);

            int aMonth = int.Parse(actual[1]);
            int eMonth = int.Parse(expected[1]);

            int aYear = int.Parse(actual[2]);
            int eYear = int.Parse(expected[2]);

            if(aYear < eYear){
                Console.WriteLine(0);
            } else if(aYear == eYear){
                if(aMonth < eMonth){
                    Console.WriteLine(0);
                } else if(aMonth == eMonth){
                    if(aDay <= eDay){
                        Console.WriteLine(0);
                    }else{
                        Console.WriteLine(15 * (aDay - eDay));
                    }
                } else {
                    Console.WriteLine(500 * (aMonth - eMonth));
                }
            } else {
                Console.WriteLine(10000);
            }
        }
    }
}
